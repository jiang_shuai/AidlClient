package com.example.client

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import com.example.server.IMyAidlInterface
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    var aidlInterface: IMyAidlInterface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv_label.setOnClickListener {
            Toast.makeText(this, aidlInterface?.name, Toast.LENGTH_SHORT).show()
        }
        var intent = Intent();
        intent.setComponent(ComponentName("com.example.server", "com.example.server.MyService"))
        bindService(intent, object : ServiceConnection {
            override fun onServiceDisconnected(p0: ComponentName?) {
            }

            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                aidlInterface = IMyAidlInterface.Stub.asInterface(p1)
            }
        }, Context.BIND_AUTO_CREATE)
    }
}
